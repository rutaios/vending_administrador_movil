//
//  TC_Principal.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 20/03/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "TC_Principal.h"
//Progress tint color
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation TC_Principal

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)cell_setup :(NSString *)instancia tipo:(NSString *)tipo conexion:(NSString *)conexion intensidad:(NSString *)intensidad alarmas:(NSString *)alarmas nivel_llenado:(NSString *)nivel_llenado ultima_venta:(NSString *)ultima_venta venta_acumulado:(NSString *)venta_acumulada fecha_ultima_visita:(NSString *)fecha_ultima_visita fecha_ultima_conexion:(NSString *)fecha_ultima_conexion
{
    label_instancia.text = instancia;
    label_tipo.text = tipo;
    label_conexion.text = conexion;
    label_intensidad.text = intensidad;
    label_alarmas.text = alarmas;
    
    float llenado = [nivel_llenado floatValue]/100;
    if (llenado>0.5) {
        [progress_bar setProgressTintColor:UIColorFromRGB(0x17B382)];
    }
    else if (llenado>0.3)
    {
        [progress_bar setProgressTintColor:UIColorFromRGB(0xF0E548)];
    }
    else
    {
        [progress_bar setProgressTintColor:UIColorFromRGB(0xC93A3A)];
    }
    [progress_bar setProgress:llenado animated:true];
    [progress_bar setTransform:CGAffineTransformMakeScale(1.0, 1.5)];
    
    label_llenado_porcentaje.text = [NSString stringWithFormat:@"%@%%",nivel_llenado];
    label_ultima.text = fecha_ultima_visita;
}

@end
