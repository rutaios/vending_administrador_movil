//
//  CV_Articulos.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 09/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "CV_Articulos.h"
#import "OBJ_Articulo.h"
#import "CC_Articulos.h"

@interface CV_Articulos ()

@end

@implementation CV_Articulos

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    //Creacion de variables
    arreglo_articulos = [[NSMutableArray alloc]init];
    
    OBJ_Articulo * articulo1 = [[OBJ_Articulo alloc]init:@"Queso" estado:@"Rico"];
    OBJ_Articulo * articulo2 = [[OBJ_Articulo alloc]init:@"Papaya" estado:@"Rico"];
    OBJ_Articulo * articulo3 = [[OBJ_Articulo alloc]init:@"Chorizo" estado:@"Vending"];
    OBJ_Articulo * articulo4 = [[OBJ_Articulo alloc]init:@"Pollo" estado:@"Vendimia"];
    [arreglo_articulos addObject:articulo1];
    [arreglo_articulos addObject:articulo2];
    [arreglo_articulos addObject:articulo3];
    [arreglo_articulos addObject:articulo4];
    
    for (int i = 0; i<50; i++) {
        OBJ_Articulo * articulo = [[OBJ_Articulo alloc]init:[NSString stringWithFormat:@"%d",i] estado:@"bien"];
        [arreglo_articulos addObject:articulo];
    }
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arreglo_articulos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CC_Articulos * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CC_Articulo" forIndexPath:indexPath];
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    [cell collection_Cell_Setup:articulo.articulo nombre:articulo.estado];
    return cell;
}

@end
