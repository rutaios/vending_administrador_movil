//
//  CC_Articulos.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 09/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "CC_Articulos.h"

@implementation CC_Articulos
@synthesize label_articulo;
@synthesize label_estado;

-(void)collection_Cell_Setup :(NSString *)estado nombre:(NSString *)nombre_articulo
{
    label_articulo.text = nombre_articulo;
    label_estado.text = estado;
}

@end
