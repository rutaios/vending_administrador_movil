//
//  CC_Articulos.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 09/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CC_Articulos : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_estado;
@property (weak, nonatomic) IBOutlet UILabel *label_articulo;

//Constructor
-(void)collection_Cell_Setup :(NSString *)estado nombre:(NSString *)nombre_articulo;

@end
