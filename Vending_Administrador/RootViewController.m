//
//  RootViewController.m
//  UUChartView
//
//  Created by shake on 15/1/4.
//  Copyright (c) 2015年 uyiuyao. All rights reserved.
//

#import "RootViewController.h"
#import "TableViewCell.h"
#import "Cellinformacion.h"

@interface RootViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UITableView *tableinformacion;
@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    datos = [NSMutableArray arrayWithArray:@[@"hola",@"bye",@"prueba",@"objeto"]];
    
}


#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:_myTableView]) {
        return 2;
    }else{
    return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:_myTableView]) {
    return section?2:4;
    }else{
        return [datos count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"TableViewCell";
     if ([tableView isEqual:_myTableView]) {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"TableViewCell" owner:nil options:nil] firstObject];
    }
    [cell configUI:indexPath];
    
         return cell;}
 
    Cellinformacion *cell2 = [tableView dequeueReusableCellWithIdentifier:@"Cellinformacion"];
   
    return cell2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:_myTableView]) {
    return 170;
    }else{
    return 60;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:_myTableView]) {
    CGRect frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 30);
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.font = [UIFont systemFontOfSize:30];
    label.backgroundColor = [UIColor colorWithRed:(164/255.0) green:(46/255.0) blue:(46/255.0) alpha:1.000]; //[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    label.text = section ? @"Insumos":@"Ventas";
    label.textColor = [UIColor colorWithRed:164 green:46 blue:46 alpha:1.000];//[UIColor colorWithRed:0.257 green:0.650 blue:0.478 alpha:1.000];
    label.textAlignment = NSTextAlignmentCenter;
    return label;
    }
    CGRect frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 20);
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.font = [UIFont systemFontOfSize:30];
    label.backgroundColor = [UIColor colorWithRed:(164/255.0) green:(46/255.0) blue:(46/255.0) alpha:1.000];
    label.text = @"Estadisticas";
    label.textColor = [UIColor colorWithRed:164 green:46 blue:46 alpha:1.000];;
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)Button1:(id)sender {
    NSLog(@"Button1");
}

- (IBAction)Button2:(id)sender {
    NSLog(@"Button2");
}

- (IBAction)Button3:(id)sender {
    NSLog(@"Button3");
}
@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net 
