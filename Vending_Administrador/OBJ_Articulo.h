//
//  OBJ_Articulo.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 09/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Articulo : NSObject
@property NSString * articulo;
@property NSString * estado;

//Constructor
-(id)init :(NSString *)nombre_articulo estado:(NSString *)nombre_estado;

@end
