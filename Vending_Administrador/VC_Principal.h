//
//  VC_Principal.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 20/03/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface VC_Principal : UIViewController <UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate>
{
    __weak IBOutlet UITableView *myTableview;
    NSMutableArray * arreglo_objetos;
    NSString * modo_ordenado;
    NSString * orden;
    __weak IBOutlet UIScrollView *scrollView;
    
    //Variables detalles
    __weak IBOutlet MKMapView *map_view;
    __weak IBOutlet UILabel *label_titulo;
    __weak IBOutlet UILabel *label_ubicacion;
    __weak IBOutlet UILabel *label_ultima_venta;
    __weak IBOutlet UILabel *label_ultima_conexion;
    __weak IBOutlet UILabel *label_venta_acumulada;
    __weak IBOutlet UILabel *label_venta_mes;
    __weak IBOutlet UILabel *label_venta_semana;
    __weak IBOutlet UILabel *label_venta_dia;
    
}
- (IBAction)orden_instancia:(id)sender;
- (IBAction)orden_tipo:(id)sender;
- (IBAction)orden_conexion:(id)sender;
- (IBAction)orden_intensidad:(id)sender;
- (IBAction)orden_alarmas:(id)sender;
- (IBAction)orden_nivel_llenado:(id)sender;
- (IBAction)orden_ultima_venta:(id)sender;
- (IBAction)orden_venta_acumulada:(id)sender;
- (IBAction)orden_fecha:(id)sender;

@property(nonatomic, retain) CLLocationManager *locationManager;

@end
