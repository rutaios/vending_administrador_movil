//
//  OBJ_Alerta.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 08/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Alerta : NSObject
//Variables
@property NSString * id_alerta;
@property NSString * nombre;
@property NSString * instancia;
@property NSString * descripcion;
@property NSString * condicion;

//Constructor
-(id)init :(NSString *)clave nombre:(NSString *)s_nombre instancia:(NSString *)s_instancia descripcion:(NSString *)s_descripcion condicion:(NSString *)s_condicion;

@end
