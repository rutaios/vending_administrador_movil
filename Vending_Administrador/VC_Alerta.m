//
//  VC_Alerta.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 08/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "VC_Alerta.h"
#import "OBJ_Alerta.h"
#import "TC_Alerta.h"

@interface VC_Alerta ()

@end

@implementation VC_Alerta

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables globales
    arreglo_objetos = [[NSMutableArray alloc]init];
    
    [self inicializacion_objetos];
}

//Obtener los datos
-(void)inicializacion_objetos
{
    OBJ_Alerta * alerta = [[OBJ_Alerta alloc]init:@"1" nombre:@"Alerta de Prueba" instancia:@"340" descripcion:@"Esta es solamente una alerta de prueba" condicion:@"No hay coca"];
    [arreglo_objetos addObject:alerta];
}

//Creacion de la tabla
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arreglo_objetos count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TC_Alerta * cell = (TC_Alerta *)[myTableView dequeueReusableCellWithIdentifier:@"TC_Alerta" forIndexPath:indexPath];
    OBJ_Alerta * alerta = [arreglo_objetos objectAtIndex:indexPath.row];
    [cell cell_setup:alerta.id_alerta nombre_alerta:alerta.nombre instancia:alerta.instancia];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btn_regrear:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
