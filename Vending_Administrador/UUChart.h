//
//  UUChart.h
//	Version 0.1
//  UUChart
//
//  Created by shake on 14-7-24.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UUChart.h"
#import "UUColor.h"
#import "UULineChart.h"
#import "UUBarChart.h"

typedef enum {
	UUChartLineStyle,
	UUChartBarStyle
} UUChartStyle;


@class UUChart;
@protocol UUChartDataSource <NSObject>

@required

- (NSArray *)UUChart_xLableArray:(UUChart *)chart;


- (NSArray *)UUChart_yValueArray:(UUChart *)chart;

@optional

- (NSArray *)UUChart_ColorArray:(UUChart *)chart;

- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart;

#pragma mark 折线图专享功能

- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart;


- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index;

- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index;
@end


@interface UUChart : UIView

@property (nonatomic, assign) BOOL showRange;

@property (assign) UUChartStyle chartStyle;

-(id)initwithUUChartDataFrame:(CGRect)rect withSource:(id<UUChartDataSource>)dataSource withStyle:(UUChartStyle)style;

- (void)showInView:(UIView *)view;

-(void)strokeChart;

@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net 
