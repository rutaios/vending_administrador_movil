//
//  VC_Principal.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 20/03/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "VC_Principal.h"
#import "OBJ_Principal.h"
#import "TC_Principal.h"

@interface VC_Principal ()
@end

@implementation VC_Principal
@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    arreglo_objetos = [[NSMutableArray alloc]init];
    [self creacion_objetos];
    modo_ordenado = @"id_instancia";
    orden = @"menor_mayor";
    
    //Ajustes de vista
    //scrollView.contentSize=CGSizeMake(1024,600);
    [self mostrar_mapa];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//INICIALIZACION DE VISTA DE DETALLES
-(void)mostrar_mapa
{
    map_view.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    [self.locationManager requestWhenInUseAuthorization];
    //[self.locationManager requestAlwaysAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
    map_view.showsUserLocation = YES;
    [map_view setMapType:MKMapTypeStandard];
    [map_view setZoomEnabled:YES];
    [map_view setScrollEnabled:YES];

    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    
    //View Area
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = self.locationManager.location.coordinate.latitude;
    region.center.longitude = self.locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    [map_view setRegion:region animated:YES];
    
    NSString *address = @"WUU";
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(28.674337, -106.077633);
    MKPlacemark *mPlacemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    
    // Create an editable PointAnnotation, using placemark's coordinates, and set your own title/subtitle
    MKPointAnnotation * punto = [[MKPointAnnotation alloc]init];
    punto.coordinate = mPlacemark.coordinate;
    punto.title = address;
    punto.subtitle = @"aloha";
    
    [map_view addAnnotation:punto];
    [map_view selectAnnotation:mPlacemark animated:NO];
}

//INICIALIZACION DE LA TABLA
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [arreglo_objetos count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TC_Principal * cell = (TC_Principal *)[myTableview dequeueReusableCellWithIdentifier:@"TC_Principal" forIndexPath:indexPath];
    NSArray * objetos_ordenados = [self ordenar_objetos];
    OBJ_Principal * objeto = [objetos_ordenados objectAtIndex:indexPath.row];
    [cell cell_setup:objeto.id_instancia tipo:objeto.tipo conexion:objeto.conexion intensidad:objeto.intensidad alarmas:objeto.alarmas nivel_llenado:[NSString stringWithFormat:@"%d",objeto.nivel_llenado] ultima_venta:objeto.ultima_venta venta_acumulado:objeto.venta_acumulada fecha_ultima_visita:objeto.fecha_ultima_visita fecha_ultima_conexion:objeto.fecha_ultima_conexion];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * objetos_ordenados = [self ordenar_objetos];
    OBJ_Principal * objeto = [objetos_ordenados objectAtIndex:indexPath.row];
    label_titulo.text = [NSString stringWithFormat:@"Máquina %@ - %@", objeto.id_instancia, objeto.tipo];
}

//MANEJADOR DE OBJETOS
-(void)creacion_objetos
{
        OBJ_Principal * objeto_1 = [[OBJ_Principal alloc]init:@"1" tipo:@"hola" conexion:@"0" intensidad:@"" alarmas:@"" nivel_llenado:@"10" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_2 = [[OBJ_Principal alloc]init:@"2" tipo:@"bye" conexion:@"1" intensidad:@"" alarmas:@"" nivel_llenado:@"20" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_3 = [[OBJ_Principal alloc]init:@"3" tipo:@"prueba" conexion:@"0" intensidad:@"" alarmas:@"" nivel_llenado:@"30" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_4 = [[OBJ_Principal alloc]init:@"4" tipo:@"objeto" conexion:@"1" intensidad:@"" alarmas:@"" nivel_llenado:@"40" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_5 = [[OBJ_Principal alloc]init:@"1" tipo:@"hola" conexion:@"0" intensidad:@"" alarmas:@"" nivel_llenado:@"50" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_6 = [[OBJ_Principal alloc]init:@"2" tipo:@"bye" conexion:@"1" intensidad:@"" alarmas:@"" nivel_llenado:@"60" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_7 = [[OBJ_Principal alloc]init:@"3" tipo:@"prueba" conexion:@"0" intensidad:@"" alarmas:@"" nivel_llenado:@"70" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_8 = [[OBJ_Principal alloc]init:@"4" tipo:@"objeto" conexion:@"1" intensidad:@"" alarmas:@"" nivel_llenado:@"80" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_9 = [[OBJ_Principal alloc]init:@"1" tipo:@"hola" conexion:@"0" intensidad:@"" alarmas:@"" nivel_llenado:@"90" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
        OBJ_Principal * objeto_10 = [[OBJ_Principal alloc]init:@"2" tipo:@"bye" conexion:@"1" intensidad:@"" alarmas:@"" nivel_llenado:@"100" ultima_venta:@"" venta_acumulada:@"" fecha_ultima_visita:@"" fecha_ultima_conexion:@""];
    
        [arreglo_objetos addObject:objeto_1];
        [arreglo_objetos addObject:objeto_2];
        [arreglo_objetos addObject:objeto_3];
        [arreglo_objetos addObject:objeto_4];
        [arreglo_objetos addObject:objeto_5];
        [arreglo_objetos addObject:objeto_6];
        [arreglo_objetos addObject:objeto_7];
        [arreglo_objetos addObject:objeto_8];
        [arreglo_objetos addObject:objeto_9];
        [arreglo_objetos addObject:objeto_10];
}

-(NSArray *)ordenar_objetos
{
    NSSortDescriptor *sortDescriptor;
    BOOL ascending = true;
    if ([orden isEqualToString:@"mayor_menor"]) {
        ascending = false;
    }
    else
    {
        ascending = true;
    }
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:modo_ordenado
                                                 ascending:ascending];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    return sortedArray = [arreglo_objetos sortedArrayUsingDescriptors:sortDescriptors];
}

-(void)designar_orden
{
    if ([orden isEqualToString:@"mayor_menor"]) {
        orden = @"menor_mayor";
    }
    else
    {
        orden = @"mayor_menor";
    }
}

-(void)table_reload
{
    NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:myTableview]);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [myTableview reloadSections:sections withRowAnimation:UITableViewRowAnimationMiddle];
}
- (IBAction)orden_instancia:(id)sender {
    modo_ordenado = @"id_instancia";
    [self designar_orden];
    [self table_reload];
}

- (IBAction)orden_tipo:(id)sender {
    modo_ordenado = @"tipo";
    [self designar_orden];
    [self table_reload];
}

- (IBAction)orden_conexion:(id)sender {
    modo_ordenado = @"conexion";
    [self designar_orden];
    [self table_reload];
}

- (IBAction)orden_intensidad:(id)sender {
}

- (IBAction)orden_alarmas:(id)sender {
}

- (IBAction)orden_nivel_llenado:(id)sender {
    modo_ordenado = @"nivel_llenado";
    [self designar_orden];
    [self table_reload];
}

- (IBAction)orden_ultima_venta:(id)sender {
}

- (IBAction)orden_venta_acumulada:(id)sender {
}

- (IBAction)orden_fecha:(id)sender {
}
@end
