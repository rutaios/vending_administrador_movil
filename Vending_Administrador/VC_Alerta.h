//
//  VC_Alerta.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 08/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_Alerta : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *myTableView;
    NSMutableArray * arreglo_objetos;
        
}
- (IBAction)btn_regrear:(id)sender;

@end
