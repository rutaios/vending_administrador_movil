//
//  TC_Principal.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 20/03/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Principal : UITableViewCell
{
    __weak IBOutlet UILabel *label_instancia;
    __weak IBOutlet UILabel *label_tipo;
    __weak IBOutlet UILabel *label_conexion;
    __weak IBOutlet UILabel *label_intensidad;
    __weak IBOutlet UILabel *label_alarmas;
    __weak IBOutlet UILabel *label_llenado;
    __weak IBOutlet UILabel *label_llenado_porcentaje;
    __weak IBOutlet UILabel *label_ultima;
    __weak IBOutlet UIProgressView *progress_bar;
    
    __weak IBOutlet UIView *view;
    
}

-(void)cell_setup :(NSString *)instancia tipo:(NSString *)tipo conexion:(NSString *)conexion intensidad:(NSString *)intensidad alarmas:(NSString *)alarmas nivel_llenado:(NSString *)nivel_llenado ultima_venta:(NSString *)ultima_venta venta_acumulado:(NSString *)venta_acumulada fecha_ultima_visita:(NSString *)fecha_ultima_visita fecha_ultima_conexion:(NSString *)fecha_ultima_conexion;

@end
