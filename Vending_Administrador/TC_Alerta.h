//
//  TC_Alerta.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 08/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Alerta : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_id;
@property (weak, nonatomic) IBOutlet UILabel *label_nombre;
@property (weak, nonatomic) IBOutlet UILabel *label_instancia;

//Constructor
-(void)cell_setup :(NSString *)id_alerta nombre_alerta:(NSString *)nombre_alerta instancia:(NSString *)instancia;
@end
