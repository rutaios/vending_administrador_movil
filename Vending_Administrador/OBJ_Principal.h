//
//  OBJ_Principal.h
//  Vending_Administrador
//
//  Created by Miguel Banderas on 20/03/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Principal : NSObject
@property NSString * id_instancia;
@property NSString * tipo;
@property NSString * conexion;
@property NSString * intensidad;
@property NSString * alarmas;
@property int nivel_llenado;
@property NSString * ultima_venta;
@property NSString * venta_acumulada;
@property NSString * fecha_ultima_visita;
@property NSString * fecha_ultima_conexion;

-(id)init :(NSString *)instancia tipo:(NSString *)nombre_tipo conexion:(NSString *)bit_conexion intensidad:(NSString *)numero_intensidad alarmas:(NSString *)numero_alarmas nivel_llenado:(NSString *)porcentaje_llenado ultima_venta:(NSString *)fecha_ultima_venta venta_acumulada:(NSString *)numero_venta_acumulada fecha_ultima_visita:(NSString *)ultima_visita fecha_ultima_conexion:(NSString *)ultima_conexion;

@end
