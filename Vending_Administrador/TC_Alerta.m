//
//  TC_Alerta.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 08/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "TC_Alerta.h"

@implementation TC_Alerta

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


//Inicializacion de la celda
-(void)cell_setup :(NSString *)id_alerta nombre_alerta:(NSString *)nombre_alerta instancia:(NSString *)instancia
{
    self.label_id.text = id_alerta;
    self.label_nombre.text = nombre_alerta;
    self.label_instancia.text = instancia;
}

@end
