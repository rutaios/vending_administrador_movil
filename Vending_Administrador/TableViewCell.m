//
//  TableViewCell.m
//  UUChartView
//
//  Created by shake on 15/1/4.
//  Copyright (c) 2015年 uyiuyao. All rights reserved.
//

#import "TableViewCell.h"
#import "UUChart.h"

@interface TableViewCell ()<UUChartDataSource>
{
    NSIndexPath *path;
    UUChart *chartView;
}
@end

@implementation TableViewCell



- (void)configUI:(NSIndexPath *)indexPath
{
    if (chartView) {
        [chartView removeFromSuperview];
        chartView = nil;
    }
    
    path = indexPath;
    
    chartView = [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(10, 10, [UIScreen mainScreen].bounds.size.width-205, 150)
                                              withSource:self
                                               withStyle:indexPath.section==1?UUChartBarStyle:UUChartLineStyle];
    [chartView showInView:self.contentView];
}

#pragma mark - @required

- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    NSString *title = @"Diciembre,Noviembre,Octubre,Septiembre,Agosto";
    [self Creartituloejex:title];
    //ejex = [NSMutableArray arrayWithArray:@[@"Enero",@"Febrero",@"Marzo",@"Abril",@"Mayo"]];
    
    
    if (path.row==0 && path.section==0) {
        return ejex;// arreglo con los parametros de los ejes x
    }
    return @[@"Enero",@"Febrero",@"Marzo",@"Abril",@"Mayo",@"Junio",@"Julio"]; //arreglo con los parametros del eje x
}

-(void) Creartituloejex:(NSString *)Titulo{
NSArray *items = [Titulo componentsSeparatedByString:@","];
    ejex = [NSMutableArray arrayWithArray:items];
}

- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{
    ejey = [NSMutableArray arrayWithArray:@[@"10",@"50",@"20",@"20",@"20"]];
    datos = [NSMutableArray arrayWithArray:@[@"22",@"54",@"15",@"30",@"42",@"77",@"43"]];
    datos2 = [NSMutableArray arrayWithArray:@[@"76",@"34",@"54",@"23",@"16",@"32",@"17"]];
    datos3 = [NSMutableArray arrayWithArray:@[@"3",@"12",@"25",@"55",@"52"]];
    
    NSArray *ary = ejey;
    NSArray *ary1 = datos;
    NSArray *ary2 = datos2;
    NSArray *ary3 = datos3;
    
    
   if (path.section==0) {
        switch (path.row) {
            case 0:
                return @[ary];
            case 1:
                return @[ary1];
            case 2:
                return @[ary1,ary2];
            default:
                return @[ary1,ary2,ary3];
        }
    }else{
        if (path.row) {
            return @[ary1,ary2];
        }else{
            return @[ary1];
        }
    }
    
//    if (path.row==0) {
//        return @[ary];
//    }
//    else if (path.row==1) {
//        return @[ary1];
//    }
//    else if (path.row==2){
//        return @[ary1,ary2];
//    }
//    else{
//        return @[ary1,ary2,ary3];
//    }
}
#pragma mark - @optional

- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    return @[UUGreen,UURed,UUBrown];
}

- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
    if (path.row==0) {
        return CGRangeMake(60, 10);
    }
    if (path.row==2) {
        return CGRangeMake(100, 0);
    }
    return CGRangeZero;
}

#pragma mark 折线图专享功能


- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart
{
    if (path.row==2) {
        return CGRangeMake(25, 75);
    }
    return CGRangeZero;
}

- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index
{
    return YES;
}


- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return path.row==2;
}
@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net 
