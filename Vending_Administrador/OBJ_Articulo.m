//
//  OBJ_Articulo.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 09/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "OBJ_Articulo.h"

@implementation OBJ_Articulo
@synthesize articulo;
@synthesize estado;

-(id)init :(NSString *)nombre_articulo estado:(NSString *)nombre_estado
{
    self.articulo = nombre_articulo;
    self.estado = nombre_estado;
    
    return self;
}

@end
