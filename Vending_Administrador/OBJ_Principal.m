//
//  OBJ_Principal.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 20/03/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "OBJ_Principal.h"

@implementation OBJ_Principal
@synthesize id_instancia;
@synthesize tipo;
@synthesize conexion;
@synthesize intensidad;
@synthesize alarmas;
@synthesize nivel_llenado;
@synthesize ultima_venta;
@synthesize venta_acumulada;
@synthesize fecha_ultima_visita;
@synthesize fecha_ultima_conexion;

-(id)init :(NSString *)instancia tipo:(NSString *)nombre_tipo conexion:(NSString *)bit_conexion intensidad:(NSString *)numero_intensidad alarmas:(NSString *)numero_alarmas nivel_llenado:(NSString *)porcentaje_llenado ultima_venta:(NSString *)fecha_ultima_venta venta_acumulada:(NSString *)numero_venta_acumulada fecha_ultima_visita:(NSString *)ultima_visita fecha_ultima_conexion:(NSString *)ultima_conexion
{
    self.id_instancia = instancia;
    self.tipo = nombre_tipo;
    self.conexion = bit_conexion;
    self.alarmas = numero_alarmas;
    self.intensidad = numero_intensidad;
    self.nivel_llenado = [porcentaje_llenado integerValue];
    self.ultima_venta = fecha_ultima_venta;
    self.venta_acumulada = numero_venta_acumulada;
    self.fecha_ultima_visita = ultima_visita;
    self.fecha_ultima_conexion = ultima_conexion;
    
    return self;
}

@end
