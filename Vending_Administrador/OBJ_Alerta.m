//
//  OBJ_Alerta.m
//  Vending_Administrador
//
//  Created by Miguel Banderas on 08/04/15.
//  Copyright (c) 2015 Null. All rights reserved.
//

#import "OBJ_Alerta.h"

@implementation OBJ_Alerta
@synthesize id_alerta;
@synthesize nombre;
@synthesize instancia;
@synthesize descripcion;
@synthesize condicion;

-(id)init :(NSString *)clave nombre:(NSString *)s_nombre instancia:(NSString *)s_instancia descripcion:(NSString *)s_descripcion condicion:(NSString *)s_condicion
{
    self.id_alerta = clave;
    self.nombre = s_nombre;
    self.instancia = s_instancia;
    self.descripcion = s_descripcion;
    self.condicion = s_condicion;
    
    return self;
}

@end
